import java.util.Scanner;

public class ApproxSine {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);

		System.out.println("Enter a value of x in sin(x): ");
		double x = input.nextDouble();
		
		double sine = 0;
		for (int i = 0; i < 5; i++) 
			sine += (Math.pow(x, 2 * i + 1) / factorial(2 * i + 1) * Math.pow(-1, i));
		

		System.out.println("The approx sine value is " + sine);
	}

	public static int factorial(int n) {

		int total = 1;
		for (int i = 1; i <= n; i++) {

			total *= i;
		}

		return total;	
	}
}