#include <stdio.h>

int main() {

    double est = 0.0;
    double step = 0.1;

    double next = est + step;

    while(step >= 0.0000001) {

        while (next * next < 2.0) {
            est = next;
            printf("%.7f \n", est);
            next = est + step;
        }

        step = step / 10.0;
        next = est + step;
    }

    return 0;
}