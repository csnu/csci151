#include <stdio.h>

int main(int argc, char const *argv[])
{
	
	int f0 = 0;
	int f1 = 1; 
	int f2 = f0 + f1;

	printf("%d %d ", f0, f1);

	while (f2 < 20) {
		printf("%d ", f2);
		f0 = f1;
		f1 = f2;
		f2 = f0 + f1;
	}
	return 0;
}