#include <stdio.h> 

int main() {

	int score;
	char ch;
	printf("Enter your score: ");
	scanf("%d", &score);

	while (1) {
		if (score >= 95) 
			ch = 'A';
		else if (score >= 80) 
			ch = 'B';
		else if (score >= 70) 
			ch = 'C';
		else 
			ch = 'D';

		if (score < 0) 
			break;

		printf("Your score is %c", ch);
		printf("\nEnter your score: ");
		scanf("%d", &score);
	}
	
	printf("Process terminated");
	
	return 0;
}