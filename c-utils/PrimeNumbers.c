#include <stdio.h>

int main(int argc, char const *argv[])
{

	_Bool isPrime;
	int count = 0;

	int i, divisor;
	for (i = 1; i < 50; i++) {
		isPrime = 1;
		for (divisor = 2; divisor < i && isPrime; divisor++) {
			if (i % divisor == 0) {
				isPrime = 0;
				break;
			}
		}
		if (isPrime) {
			count++;
			if (count % 10 == 0)
				printf("%d\n", i);
			else
				printf("%d ", i);
		}
	}
	return 0;
}