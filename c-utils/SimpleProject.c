#include <stdio.h>

int main(int argc, char const *argv[])
{
	int x;
	printf("Enter an integer \n");
	scanf("%i", &x);

	printf("The octal value is %o and hex value is %x \n", x, x);
	
	return 0;
}