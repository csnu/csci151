#include <stdio.h>
#include <math.h>

int main() {

	double input;
	printf("Enter a value (enter negative to quit): ");

	scanf("%lf", &input);

	while (input >= 0) {

		double output = sqrt(input);
		printf("The square root of %f is %f", input, output);

		printf("\nEnter a value (enter negative to quit): ");

		scanf("%lf", &input);
	} 

	printf("Process terminated");

	return 0;
}