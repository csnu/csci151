#include <stdio.h>

int main(int argc, char const *argv[])
{
	
	int array[10];
	int factorial = 1;
	int i;
	
	for (i = 1; i <= 10; i++) {
		
		array[i-1] = factorial;
		factorial *= i;
		printf("%i! = %i\n", i - 1, array[i-1]);
	}

	return 0;
}