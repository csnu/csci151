#include <stdio.h>

int main(int argc, char const *argv[])
{
	
	double array[10];

	int i;

	for (i = 0; i < 10; i++) 
		array[i] = i * 3.14159 / 2.0;

	for (i = 0; i < 12; i++)
		printf("PI / 2 * %i is %f\n", i, array[i]);

	int even[50];

	for (i = 0; i < 50; i++) {
		even[i] = i * 2;
	}
		

	for (i = 0; i < 50; i++) {
		
		printf("even[%i] = %i\n", i, even[i]);	
		
	}

	return 0;
}