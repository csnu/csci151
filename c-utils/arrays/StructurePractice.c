#include <stdio.h>

struct time {

	int hours;
	int minutes;
	char amORpm; 
};

int main(int argc, char const *argv[])
{
	struct time t = {11, 23, 'a'};

	printf("Time is %i:%i", t.hours, t.minutes);

	if (t.amORpm == 'a')
		printf(" in the morning");
	else
		printf(" in the afternoon");
	return 0;
}