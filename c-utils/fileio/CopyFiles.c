#include <stdio.h>

int main(int argc, char const *argv[])
{
	
	FILE *infile;
	FILE *outfile;

	infile = fopen("CopyFiles.c", "r");
	outfile = fopen("OutputFile.txt", "w");

	if (infile == NULL || outfile == NULL) {
		printf("Problem opening files.");
		return 1;
	}

	printf("file opened successfully");
	char ch;

	do {

		ch = getc(infile);
		fprintf(outfile, "%c", ch);
	} while (!feof(infile));
	return 0;
}