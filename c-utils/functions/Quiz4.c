#include <stdio.h>

int f1(int x) {

	int y = x;

	if (x <= 0) {
		return 3;
	}
	else {
		printf("%i\n", x);
		y = 3 + x + f1(x-1);
		printf("%i\n", y);
		return y;
	}
}
int main(int argc, char const *argv[])
{
	
	int x = f1(4);

	printf("%i", x);
	return 0;
}