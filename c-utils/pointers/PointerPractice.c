#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void copy(char source[], char target[], int n) {

	int i;

	for (i = 0; i < n; i++)
		target[i] = source[i];
}

int main(int argc, char const *argv[])
{
	int val1 = 40, val2 = 50, val3 = 60;

	int *ptr1 = &val1;
	int *ptr2 = &val2;
	int *ptr3 = &val3;

	*ptr2 = *ptr3;
	*ptr2 = 10;
	ptr1 = ptr2;

	printf("%d %d %d %d\n", ptr1, *ptr1, *ptr2, *ptr3);
	printf("%d\n", ptr2);

	char s1[] = "Astana";
	char s2[] = "Actana";

	printf("%i\n", strcmp(s1, s2));

	printf("%i %f\n", atoi("73.12"), atof("521.21e-3e2"));
	printf("%li %f\n", atol("521.21e-3e2"), atof("521.21e-3e2"));

	copy(s1, s2, 6);

	printf("%s", s1);
	
	char str1[20] = "ush";
	char str2[20] = "bes";

	strcat(str2, str1);
	printf("%s %s", str1, str2);
	return 0;
}