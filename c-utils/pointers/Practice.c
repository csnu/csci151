#include <stdio.h>

int main(int argc, char const *argv[])
{
	/*
	char ch1 = 'X', ch2 = 'Y';
	char ch3 = 'Z', ch4 = 'W';
	
	char *chp1 = &ch1;
	char *chp2;
	
	*chp1 = 'A';
	chp2 = chp1;
	
	ch3 = *chp2;
	chp2 = &ch4;
	
	ch1 = 'B';
	chp1 = &ch2;

	printf("ch1 = %c\n", ch1);
	printf("ch2 = %c\n", ch2);
	printf("ch3 = %c\n", ch3);
	printf("ch4 = %c\n", ch4);

	printf("chp1 = %c\n", *chp1);
	printf("chp2 = %c\n", *chp2);

	printf("&ch1 = %p\n", &ch1);
	printf("&ch2 = %p\n", &ch2);
	printf("&ch3 = %p\n", &ch3);
	printf("&ch4 = %p\n", &ch4);

	printf("&chp1 = %p\n", chp1);
	printf("&chp2 = %p\n", chp2);
	*/

		int a = 11, b = 20;
	int x, y;
	
	int *ip1 = &a;
	int *ip2 = &x;
	
	int **ipp = &ip2;
	
	*ip2 = *ip1 * 7 + b;
	
	ip1 = ip2;
	ip2 = &y;
	
	**ipp = 88;
	*ipp = &b;


	printf("a = %i\n", a);
	printf("b = %i\n", b);
	printf("x = %i\n", x);
	printf("y = %i\n", y);

	printf("*ip1 = %i\n", *ip1);
	printf("*ip2 = %i\n", *ip2);
	printf("**ipp = %i\n", **ipp);

	printf("&a = %p\n", &a);
	printf("&b = %p\n", &b);
	printf("&x = %p\n", &x);
	printf("&y = %p\n", &y);

	printf("ip1 = %p\n", ip1);
	printf("ip2 = %p\n", ip2);
	printf("ipp = %p\n", ipp);
	printf("*ipp = %p\n", *ipp);
	//printf("a = %i\n", a);
	return 0;
}