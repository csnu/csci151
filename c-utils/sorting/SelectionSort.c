#include <stdio.h>

void sort(int array[], int size) {

	int i, j;

	int index;
	int min = array[0];

	for (i = 0; i < size; i++) {
		index = i;
		min = array[i];
		for (j = i + 1; j < size; j++) {

			if (min > array[j]) {
				min = array[j];
				index = j;
			}
		}

		if (index != i) {
			array[index] = array[i];
			array[i] = min;
		}
	}
}

void print(int array[],  int size) {
	int i;

	for (i = 0; i < size; i++) {
		printf("%i ", array[i]);
	}
}
int main(int argc, char const *argv[])
{
	
	int array[] = {2, 1, -1, 4, 3, 7, 5};

	sort(array, 7);

	print(array, 7);
	return 0;
}