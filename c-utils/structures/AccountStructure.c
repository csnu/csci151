#include <stdio.h>


typedef struct {
	int id;
	_Bool isSavings;
	int balance;
	double interestRate;
} account; 

int main(int argc, char const *argv[])
{

	account myAccount;

	printf("Enter costumer's id: ");
	scanf("%i", &myAccount.id);

	char ch;
	printf("Is this a savings account? (y/n): ");
	scanf(" %c", &ch);

	if (ch == 'y' || ch == 'Y') {
		myAccount.isSavings = 1;
		myAccount.interestRate = 0.02;
	} else {
		myAccount.isSavings = 0;
		myAccount.interestRate = 0.01;
	}

	printf("What is the initial balance? ");
	scanf("%i", &myAccount.balance);

	int years;
	printf("How many years will you wait? ");
	scanf("%i", &years);

	int i;
	for (i = 0; i < years; i++) {
		myAccount.balance *= (1.0 + myAccount.interestRate);
	}

	printf("Your final balance is $%i", myAccount.balance);
	return 0;
}