#include <stdio.h>

typedef struct {

	int r, g, b;
} Color;

typedef struct {
	int x, y;
} Point;
/*
typedef struct {

	double radius;
	Point center;
	Color outsideColor;
	Color fillColor;
} Circle;
*/

typedef struct {

	Point point1;
	Point point2;
	Color color;
} LineSegment;

int main(int argc, char const *argv[])
{
	
	Color white = {255, 255, 255};
	Color red = {255, 0, 0};
	Color black = {0, 0, 0};

	LineSegment polygon[5] = {

			{{12,12}, {23, 45}, white},
			{{12,12}, {23, 45}, red},
			{{143,12}, {23, 45}, black},
			{{12,12}, {23, 45}, white},
			{{12,12}, {23, 45}, white}
	};
	int i;
	for (i = 0; i < 5; i++) {
		printf("Polygon No. %i has (%i,%i) coordinate values\n", (i + 1), 
			polygon[i].point1.x, polygon[i].point1.y);
	}
	/*
	Point p1 = {12, 33};
	LineSegment line;;

	line.color = white;
	line.point1 = pt1;

	line.point2.x = 65;
	line.point2.y = 12;
*/
	return 0;
}