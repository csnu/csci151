#include <stdio.h>

typedef struct  {

	int hours;
	int minutes;
	char amORpm; 
} time;

int main(int argc, char const *argv[])
{
	time t;
	t = (time) {11, 23, 'a'};

	time midnight = {12, 2, 'p'};
	t.hours = 5;
	t.amORpm = 'p';

	printf("Time is %i:%i", midnight.hours, midnight.minutes);

	if (t.amORpm == 'a')
		printf(" in the morning");
	else
		printf(" in the afternoon");

	
	return 0;
}