#include <stdio.h>
#include <stdlib.h>

typedef struct _weather {

	int rain;
	double temp;
	double wind;
	struct _weather *link;
} weather;

int getTotalRainfall(weather *node) {

	if (node != NULL) {
		return node->rain + getTotalRainfall(node->link);
	}
}

float getLowestTemp(weather *node) {

	if (node != NULL) {

		float min = getLowestTemp(node->link);

		if (min < node->temp)
			return min;
		else
			return node->temp;

	}
}

float getAverageWindSpeed(weather *node) {

	int n = 0;
	float total = 0;

	while (node != NULL) {
		n++;
		total += node->wind;
		node = node->link;
	}

	return total / n;
}

void displayTable(weather *node) {
	
	if (node != NULL) {
		printf("%i\t%.1f\t%.1f\n", node->rain, node->temp, node->wind);
		displayTable(node->link);
	}
}

int main(void) {

	FILE *file = fopen("astana.txt", "r");

	weather *first = NULL;
	weather *prev = NULL;
	
	int rain;
	float temp, wind;

	while (!feof(file)) {
			 
		weather *newNode = malloc(sizeof(weather));;

		fscanf(file, "%i %f %f", &rain, &temp, &wind);
		
		newNode->rain = rain;
		newNode->temp = temp;
		newNode->wind = wind;

		newNode->link = NULL;

		if (first == NULL) {
			first = newNode;
		} else {
			prev->link = newNode;
		}

		prev = newNode;
	}

	displayTable(first);

	printf("\nTotal rainfall for the entire year: %i cm\n", getTotalRainfall(first));
	
	printf("The lowest average temperature: %.1f degrees in Celsius\n", getLowestTemp(first));

	printf("The average wind speed: %.2f m/s\n", getAverageWindSpeed(first));

	return EXIT_SUCCESS;
}
