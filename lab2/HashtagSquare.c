#include <stdio.h>

int main() {

	setvbuf(stdout, NULL, _IONBF, 0);
	
	printf("Enter a number for the length of the side: ");
	int n;

	scanf("%d", &n);

	int i, j;

	for (i = 0; i < n; i++) {

		for (j = 0; j < n; j++) 
			printf("#");
		printf("\n");
	}
}