/*
 ============================================================================
 Name        : HarmonicSum.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {	

	setvbuf(stdout, NULL, _IONBF, 0);

	int x;

	printf("Enter number of terms: ");
	scanf("%i", &x);

	double total = 0;
	int i;
	for (i = 1; i <= x; i++) {
		total += 1.0 / i;
		printf("1/%d", i);
		if (i == x)
			printf("=");
		else
			printf("+");
	}

	printf("%lf", total);
	printf("\nTotal is %lf", total);
	return EXIT_SUCCESS;
}
