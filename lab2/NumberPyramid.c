
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	int n;

	printf("Enter a number: \n ");
	scanf("%i", &n);

	int i, j;
	for (i = n; i > 0; i--) {

		for (j = i; j > 0; j--) {
			printf("%d ", j);

		}
		printf("\n");
	}
	return EXIT_SUCCESS;
}
