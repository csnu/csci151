/*
 ============================================================================
 Name        : Frequency.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {

	setvbuf(stdout, NULL, _IONBF, 0);

	printf("Enter a long number: ");
	int value;

	scanf("%d", &value);
	int constantValue = value;
	int i;
	int lastDigit;
	int count = 0;

	for (i = 0; i < 10; i++) {
		count = 0;
		value = constantValue;

		while (value > 0) {

			lastDigit = value % 10;

			if (lastDigit == i) {
				count++;
			}

			value /= 10;
		}
		printf("\nDigit %d appears %d times", i, count);
	}
	return EXIT_SUCCESS;
}
