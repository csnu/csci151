/*
 ============================================================================
 Name        : PrimeNumber.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	setvbuf(stdout, NULL, _IONBF, 0);

	printf("Please enter a number (negative number to quit): ");
	int value;

	scanf("%d", &value);

	_Bool isPrime;

	while (value >= 0) {
		isPrime = 1;
		int divisor;
		for (divisor = 2; divisor < value; divisor++) {

			if (value % divisor == 0) {
				isPrime = 0;
				break;
			}
		}

		if (isPrime)
			printf("It is a prime number!\n");

		printf("Please enter a number (negative number to quit): ");
		scanf("%d", &value);
	}

	printf("Process terminated");
	return EXIT_SUCCESS;
}
