/*
 ============================================================================
 Name        : SimpleGame.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {

	setvbuf(stdout, NULL, _IONBF, 0);

	int value;
	int sum = 0;
	do {
		printf("Please enter a number: ");
		scanf("%i", &value);
		if (value % 2 != 0) {
					printf("You entered %i . It is an odd number. I will terminate. The sum is %i", value, sum);
					break;
		}

		sum += value;

	} while (1);
	return EXIT_SUCCESS;
}
