/*
 ============================================================================
 Name        : Files.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void) {

	time_t t;
	time(&t);
	FILE *file;
	file = fopen("Log.txt", "w");

	if (file == NULL) {
		printf("Problem opening the file");
		return 1;
	}

	char name[100];
	int age;

	scanf("%s %i", name, &age);


	fprintf(file, "You entered name = %s, age = %i on %s", name, age, ctime(&t));

	fclose(file);

	return EXIT_SUCCESS;
}
