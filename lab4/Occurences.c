/*
 ============================================================================
 Name        : Occurences.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {

	char ch;
	int i = 0;
	int counts[26];

	for (i = 0; i < 26; i++)
		counts[i] = 0;

	do {
		ch = getchar();
		if (ch != ' ' || ch != '\n')
			counts[ch - 'a']++;
	} while (ch != '\n');

	for (i = 0; i < 26; i++) {
		if (counts[i] > 0)
			printf("%c has %i occurences \n", (char) (i + 'a'), counts[i]);
	}

	return EXIT_SUCCESS;
}
