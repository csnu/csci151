/*
 ============================================================================
 Name        : SimpleGame.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void) {

	setvbuf(stdout, NULL, _IONBF, 0);

	char ch;
	printf("Please enter s or r: ");

	ch = getchar();

	int number;

	if (ch == 'r') {
		number = rand() % 10;
		printf("You called the pseudorandom function, "
				"it has generated the number: %i", number);
	}
	else {
		int seed;
		printf("Enter a seed for random number: ");
		scanf("%i", &seed);
		srand(seed);
		number = rand() % 10;
		printf("It is %i", number);
	}

	return EXIT_SUCCESS;
}
