#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main() {

	int size = 5;
	int matrix[size][size];

	int i,j;
	int max = matrix[0][0];

	srand(time(NULL));

	for (i = 0; i < size; i++) {
		
		for (j = 0; j < size; j++) {
				
			matrix[i][j] = rand() % 50;
			
			if (max < matrix[i][j])
				max = matrix[i][j];
		}	
	}

	for (i = 0; i < size; i++) {
		
		for (j = 0; j < size; j++) {
		
			printf("%2i ", matrix[i][j]);
		}
		printf("\n");	
	}

	printf("The maximum value in the matrix is %i", max);
	return 0;
}