#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main() {

	int size = 5;
	int matrix[size][size];

	int i,j;
	srand(time(NULL));
	for (i = 0; i < size; i++) {
		
		for (j = 0; j < size; j++) {
		
			matrix[i][j] = rand() % 50;
		}	
	}

	for (i = 0; i < size; i++) {
		
		for (j = 0; j < size; j++) {
		
			printf("%2i ", matrix[i][j]);
		}
		printf("\n");	
	}
	return 0;
}