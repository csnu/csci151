#include <stdio.h>


typedef struct {

	int id;
	char gender;
	double quizGrade;
	double labGrade;
	double midtermGrade;
	double totalGrade;
} Student; 

int main(int argc, char const *argv[])
{

	setvbuf(stdout, NULL, _IONBF, 0);

	Student student;
	
	printf("Enter student's id: ");
	scanf("%i", &student.id);

	printf("Enter student's gender (m/f): ");
	scanf(" %c", &student.gender);

	printf("Enter student's quiz grade: ");
	scanf("%lf", &student.quizGrade);

	printf("Enter student's lab grade: ");
	scanf("%lf", &student.labGrade);

	printf("Enter student's midterm grade: ");
	scanf("%lf", &student.midtermGrade);

	student.totalGrade = 0.2 * student.labGrade + 
		0.45 * student.midtermGrade + 0.35 * student.quizGrade;
	
	printf("The student's\n");
	printf("id: %i\n", student.id);
	printf("gender: %s\n", (student.gender == 'm' ? "male" : "female"));
	printf("quiz grade: %.1f\n", student.quizGrade);
	printf("lab grade: %.1f\n", student.labGrade);
	printf("midterm grade: %.1f\n", student.midtermGrade);
	printf("total grade: %.1f\n", student.totalGrade);

	return 0;
}