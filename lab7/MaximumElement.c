/*
 ============================================================================
 Name        : MaximumElement.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int max(int size, int array[]) {

	int max = array[0];

	int i;

	for (i = 1; i < size; i++) {
		if (max < array[i])
			max = array[i];
	}

	return max;
}

void prettyPrint(int size, int array[]) {

	printf("[");
	int i;
	for (i = 0; i < size; i++) {
		printf("%i", array[i]);
		if (i == size - 1)
			printf("]");
		else
			printf(",");
	}
}

int main(void) {

	setvbuf(stdout, NULL, _IONBF, 0);

	srand(time(NULL));

	printf("Enter a size for an array: ");
	int size;
	scanf("%i", &size);

	int array[size];
	int i;
	for (i = 0; i < size; i++) {
		array[i] = rand() % 100;
	}

	prettyPrint(size, array);
	printf("\nMaximum value in the array is %i", max(size, array));
	return EXIT_SUCCESS;
}
