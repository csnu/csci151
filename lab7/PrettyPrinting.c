/*
 ============================================================================
 Name        : PrettyPrinting.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

void prettyPrint(int size, int array[]) {

	printf("[");
	int i;
	for (i = 0; i < size; i++) {
		printf("%i", array[i]);
		if (i == size - 1)
			printf("]");
		else
			printf(",");
	}
}
int main(void) {
	int array[] = {1, 2, 3, 4, 2, 41, 325, 34};

	prettyPrint(8, array);

	return EXIT_SUCCESS;
}
