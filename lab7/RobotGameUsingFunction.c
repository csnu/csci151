#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

typedef struct {
	_Bool hasRobot; // 1 (true) a robot is here; 0 (false) the space is clear
	int robHeading; // 0 (west), 1 (north), 2 (east), 3 (south)
	_Bool robAlive; // 1 (true) for running robot; (false) when crashed
} gridSquare;

//TODO: implement the following functions

/**
 * Initialize the grid of size s so that there are no robots initially
 */
void gridInit(int s, gridSquare grid[s][s]) {

	int i;
	int j;

	for (i = 0; i < s; i++) {

		for (j = 0; j < s; j++) {
			grid[i][j].hasRobot = 0;
			grid[i][j].robHeading = 0;
			grid[i][j].robAlive = 0;
		}
	}
}

/**
 * Print the grid: put directions according to robHeading
 * and . if there is no robot at the specified cell
 */
void printGrid(int s, gridSquare grid[s][s]) {

	int i, j;

	for (i = 0; i < s; i++) {

		for (j = 0; j < s; j++) {

			if (grid[i][j].hasRobot && grid[i][j].robAlive) {

				switch (grid[i][j].robHeading) {
					case 0: printf("<"); break;
					case 1: printf("^"); break;
					case 2: printf(">"); break;
					case 3: printf("v"); break;
				}
			}
			else if (!grid[i][j].robAlive && grid[i][j].hasRobot)
					printf("@");
			else if (!grid[i][j].hasRobot)
					printf(".");
			else
					printf("Error somewhere");
		}

		printf("   %i\n", i);
	}

	for (i = 0; i < s; i++)
		printf("%i", i);
}

/**
 * Randomly add robots to the grid.
 * Make sure to generate robots only if the number robots to add can fit the grid.
 * In other words, quantity should be less than the area of the grid.
 */
void addRobots(int quantity, int size, gridSquare grid[size][size]) {

	if (quantity > size * size)
		return; 	//cannot be

	int i;
	int row, column;

	for (i = 0; i < quantity; i++) {

				// This makes sure that the grid space is currently empty
				do {
					row = rand() % size;
					column = rand() % size;
				} while (grid[row][column].hasRobot);

				// TODO:  Randomly set the heading at (column,row),
				//        and make the robot alive

				grid[row][column].hasRobot = 1;
				grid[row][column].robHeading = rand() % 4;
				grid[row][column].robAlive = 1;
		}
}

/**
 * Rotate the robot anti-clockwise.
 * Update the heading accordingly.
 * row and column here are the user input.
 */
void rotateLeft(int row, int column, int size, gridSquare grid[size][size]) {

	int heading = grid[row][column].robHeading;
	// TODO: Update the heading accordingly
	switch (heading) {
		case 0: grid[row][column].robHeading = 2; break;
		case 1: grid[row][column].robHeading = 0; break;
		case 2: grid[row][column].robHeading = 1; break;
		case 3: grid[row][column].robHeading = 2; break;
		default: printf("your random is wrong check.");
	}
}

/**
 * Rotate the robot clockwise.
 * Update the heading accordingly.
 * row and column here are the user input.
 */
void rotateRight(int row, int column, int size, gridSquare grid[size][size]) {

	int heading = grid[row][column].robHeading;

	switch (heading) {

		case 0: grid[row][column].robHeading = 1; break;
		case 1: grid[row][column].robHeading = 2; break;
		case 2: grid[row][column].robHeading = 3; break;
		case 3: grid[row][column].robHeading = 0; break;
		default: printf("your random is wrong check.");
	}
}


/**
 * OPTIONAL: move the robot at row, column forward
 */
void moveForward(int row, int column, int s, gridSquare grid[s][s]) {

	int liveCount = 10;

	switch (grid[row][column].robHeading) {

		case 0: // Pointing west
			if (column > 0) {
				// TODO: If a robot is in front of you, crash both;
				// otherwise move the robot forward
				if(grid[row][column - 1].hasRobot == 1){
					grid[row][column - 1].robAlive = 0;
					grid[row][column].robAlive = 0;
					liveCount = liveCount - 2;
				}else{
					grid[row][column - 1].hasRobot = 1;
					grid[row][column].hasRobot = 0;
					grid[row][column - 1].robAlive = 1;
					grid[row][column].robAlive = 0;
					grid[row][column - 1].robHeading = 0;
				}
		} break;

		case 1: // Pointing north
			if (row > 0) {
				if(grid[row - 1][column].hasRobot == 1){
					grid[row - 1][column].robAlive = 0;
					grid[row][column].robAlive = 0;
					liveCount = liveCount - 2;
				}else{
					grid[row - 1][column].hasRobot = 1;
					grid[row][column].hasRobot = 0;
					grid[row - 1][column].robAlive = 1;
					grid[row][column].robAlive = 0;
					grid[row - 1][column].robHeading = 1;
				}
			}
			break;

			case 2: // Pointing east
				if (column < 9) {
					if(grid[row][column + 1].hasRobot == 1){
						grid[row][column + 1].robAlive = 0;
						grid[row][column].robAlive = 0;
						liveCount = liveCount - 2;
					}else{
						grid[row][column + 1].hasRobot = 1;
						grid[row][column].hasRobot = 0;
						grid[row][column + 1].robAlive = 1;
						grid[row][column].robAlive = 0;
						grid[row][column + 1].robHeading = 2;
					}
				}
				break;
			case 3: //Pointing south
				if(row < 9) {
					if(grid[row + 1][column].hasRobot == 1){
						grid[row + 1][column].robAlive = 0;
						grid[row][column].robAlive = 0;
						liveCount = liveCount - 2;
					}else{
						grid[row + 1][column].hasRobot = 1;
						grid[row][column].hasRobot = 0;
						grid[row + 1][column].robAlive = 1;
						grid[row][column].robAlive = 0;
						grid[row + 1][column].robHeading = 3;
					}
				}
				break;

				default: // Shouldn't happen
					break;

				printf("Ok,Robot moved forward ");

	}
}



int main() {
	setvbuf(stdout, NULL, _IONBF, 0);

	// The 10 by 10 grid
	int s = 10;
	gridSquare grid[s][s];

	// TODO: initialize the grid using the gridInit function defined above
	gridInit(s, grid);


	int row, column;
	int number_of_robots = 10; //could be any number that can fit to the grid.

	// Add 10 robots to the grid using a function
	addRobots(number_of_robots, s, grid);


	// Counts the number of live robots
	int liveCount = 10;

	// main loop of the game
	do {

		// TODO: Output the grid using the printing function
		printGrid(s, grid);

		char cmd;

		// Obtain user input (move or rotate command)
		printf("\n input action and coordinates, please: ");
		scanf(" %c %i %i", &cmd, &row, &column);

		// Process user command
		if (grid[row][column].hasRobot && grid[row][column].robAlive) {

			if (cmd == 'L')
				rotateLeft(row, column, s, grid);

			else if (cmd == 'R')
				rotateRight(row, column, s, grid);

			else if (cmd == 'F')

				moveForward(row, column, s, grid);

		}

	} while (liveCount > 0);

	return 0;

}
