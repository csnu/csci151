/*
 ============================================================================
 Name        : lab8.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Recursion. 1st task
int minValue(int arr[], int from, int to) {

	int min;

	if (to == 1)
		return arr[0];
	else {
		min = minValue(arr, from, to - 1);

		if (min < arr[to - 1])
			return min;
		else
			return arr[to - 1];
	}
}

//Recursion. 2nd task
int compute(int n, int k) {

	if (n == 0 || k == n)
		return 1;
	else if (n > k)
		return compute(n - 1, k) + compute(n-1, k - 1);
	else 
		return 0;	
}

//function that compares two string
int compare(char s1[], char s2[]) {

	int i;
	int value;

	for (i = 0; s1[i] != '\0' || s2[i] != '\0'; i++) {
		if (s1[i] > s2[i]) {
			value = 1;
			break;
		}
		else if (s1[i] < s2[i]) {
			value = -1;
			break;
		}
		else
			value = 0;
	}

	return value;
}


/*variables and their values
 * a: 11
b: 20
x: 97
y: 88
ip1: address of a
ip2: address of y
ipp: address of ip2
*ip1: 97
*ip2: 20
*ipp: address of b
**ipp: 20

 * */


int main(void) {

	int value = compare("Dante", "Dantes");


	if (value == 0)
		printf("They're the same");
	else if (value > 0)
		printf("The first string is bigger");
	else
		printf("The second string is bigger");


	int array[] = {6, 2, 3, 5, -1};

	printf("\nSmallest value in the array is %i", minValue(array, 0, 5));

	printf("\nC(3,2): %i\n", compute(3, 2));


	return EXIT_SUCCESS;
}
