/*
 ============================================================================
 Name        : EncrptDecrptFile.c
 Author      : Makhambet Torezhan
 Version     :
 Copyright   : Your copyright notice
 Description : Encryption and Descryption Machine
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

_Bool isValid(char string[], char keyword[]) {

	_Bool isThisTheWord = 1;
	_Bool isTrueLetters = 1;
	int i;

	for (i = 0; i < 254 && string[i] != '\0'; i++) {
		if (string[i] != keyword[i])
			isThisTheWord = 0;
		if (string[i] != keyword[0])
			isTrueLetters = 0;
	}

	return isTrueLetters || isThisTheWord;
}

void readFile(char fileName[], char fileCharacters[]) {

	FILE *file;
	file = fopen(fileName, "r");

	if (file == NULL)
		printf("Problem opening the file");

	int i;

	for (i = 0; i < 254; i++)
		fileCharacters[i] = getc(file);

}

int findIndexInArray(char subarr[], char ch) {
	int index;

	int i;
	for (i = 0; i < 254; i++)
			if (subarr[i] == ch)
				index = i;

	return index;
}
void encrypt(FILE *fin, FILE *fout, char subarr[], char pass[]) {

	int sum = 0;

	char ch;
	int index = 0;
	ch = getc(fin);
	while (ch != EOF){

		if (pass[index] == '\0')
			index = 0;



		printf("%c", ch);

		sum = (unsigned char) ch + (int) pass[index];
		printf("%i \n", sum);

		//if (sum > 254)
				//break;


		putc(subarr[sum % 255], fout);
		ch = getc(fin);
		index++;
	}

	fclose(fin);
	fclose(fout);
}

void decrypt(FILE *fin, FILE *fout, char subarr[], char pass[]) {

	int sum;
	char ch;
	int index = 0;


	while (ch != EOF) {

		if (pass[index] == '\0')
			index = 0;

		ch = getc(fin);

		int indexForFile = findIndexInArray(subarr, ch);

		printf("%i-%i\n", indexForFile, (int) pass[index]);

		sum = indexForFile - (int) pass[index];

		if (sum < 0)
			break;

		char decryptedChar = (unsigned char) sum % 254;

		printf("%c\n", decryptedChar);
		putc(decryptedChar, fout);

		index++;
	}

	fclose(fin);
	fclose(fout);
}

int main(void) {

	setvbuf(stdout, NULL, _IONBF, 0);

	char fileCharacters[254];

	readFile("key.254", fileCharacters);

	printf("WELCOME to the encryption service!\n");

	printf("ENTER your password\n");

	char password[254];

	scanf("%s", password);

	char command[254];

	do {

		printf("MENU: (e) to encode, (d) to decode, or (q) to quit\n");
		scanf("%s", command);

		if (isValid(command, "encrypt")) {
			printf("ENTER a file to encrypt\n");
			char sourceFileName[254];
			scanf("%s", sourceFileName);

			printf("ENTER a filename for the encrypted file\n");
			char encryptedFileName[254];
			scanf("%s", encryptedFileName);

			FILE *sourceFile;
			FILE *encryptedFile;

			sourceFile = fopen(sourceFileName, "r");
			encryptedFile = fopen(encryptedFileName, "w");

			encrypt(sourceFile, encryptedFile, fileCharacters, password);
		}

		else if (isValid(command, "decrypt")) {

			printf("ENTER a file to decrypt\n");

			char sourceFileName[254];
			scanf("%s", sourceFileName);

			printf("ENTER a filename for the decrypted file\n");

			char decryptedFileName[254];
			scanf("%s", decryptedFileName);

			FILE *sourceFile;
			FILE *decryptedFile;

			sourceFile = fopen(sourceFileName, "r");
			decryptedFile = fopen(decryptedFileName, "w");

			decrypt(sourceFile, decryptedFile, fileCharacters, password);
		}
		else if (isValid(command, "quit"))
			break;
		else {
			printf("Unrecognized command\n");
			continue;
		}

	} while (1);

	printf("BYE!");
	return EXIT_SUCCESS;
}
