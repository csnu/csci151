/*
 ============================================================================
 Name        : Lesson25Ex1.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* stringCopy(char *fromStr) {

	int n = strlen(fromStr);
	char *string = (char*) malloc(n * sizeof(double));

	int i;
	for (i = 0; i <= n; i++) {

		*(string + i) = *(fromStr + i);

	}

	return string;
}
int main(void) {

	char *fromStr = "Makhamber is good boy";
	char *string = stringCopy(fromStr);

	printf("%s", string);
	return 0;
}
