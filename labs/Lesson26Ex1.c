/*
 ============================================================================
 Name        : Lesson26Ex1.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct _characters {

	char ch;
	struct _characters* link;
} characters;

void print(characters *node) {
	if (node != NULL) {
		printf("%c", node->ch);
		print(node->link);
	}
}

void printReversed(characters *node) {

	if (node != NULL) {

		printReversed(node->link);
		printf("%c", node->ch);

	}
}


int main(void) {

	 setvbuf(stdout, NULL, _IONBF, 0);

	 characters *first = NULL;
	 characters *prev = NULL;

	 char c = 'a';


	 do {

		 characters *newNode = malloc(sizeof(characters));;

		 newNode->ch = c;
		 newNode->link = NULL;

		 if (first == NULL) {
			 first = newNode;
		 } else {
			 prev->link = newNode;
		 }

		 prev = newNode;
		 c++;
	 } while (c != 'z' + 1);

	printf("Real order: ");
	print(first);

	printf("\nReversed order: ");
	printReversed(first);
	return EXIT_SUCCESS;
}
