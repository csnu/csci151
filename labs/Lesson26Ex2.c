/*
 ============================================================================
 Name        : Lesson26Ex2.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct _weather {

	int rain;
	double temp;
	double wind;
	struct _weather *link;
} weather;

void print(weather *node) {
	if (node != NULL) {
		printf("%i %f %f", node->rain, node->temp, node->wind);
		print(node->link);
	}
}

int main(void) {

	FILE *file = fopen("astana.txt", "r");

	weather *first = NULL;
	weather *prev = NULL;
	char ch;
	while (ch != EOF) {
			 ch = getc(file);
			 weather *newNode = malloc(sizeof(weather));;

			 fscanf(file, "%i %lf %lf", &newNode->rain, &newNode->temp, &newNode->wind);


			 newNode->link = NULL;

			 if (first == NULL) {
				 first = newNode;
			 } else {
				 prev->link = newNode;
			 }

			 prev = newNode;

	}

	return EXIT_SUCCESS;
}
