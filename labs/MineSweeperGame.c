/*
 ============================================================================
 Name        : MineSweeperGame.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define WIDTH 12
#define HEIGHT 30

void displayMap(char map[][HEIGHT]) {

	int i, j;

	for (i = 0; i < WIDTH; i++) {

		for (j = 0; j < HEIGHT; j++)
			printf("%c", map[i][j]);
		printf("\n");
	}
}

void generateMines(char map[][HEIGHT]) {

	int i;

	int xRandom, yRandom;

	for (i = 0; i < 10; i++) {

		 xRandom = rand() % WIDTH;
		 yRandom = rand() % HEIGHT;

		 if (map[xRandom][yRandom] != '.')
			 continue;
		 else
			 map[xRandom][yRandom] = '*';
	}
}

int getCount(int x, int y, char map[][HEIGHT]) {

	int count = 0;

	int i, j;

	for (i = x; i < 4 && i < WIDTH; i++) {

			for (j = y; j < 3 && j < HEIGHT; j++) {

				if (map[i][j] == '*')
					count++;
			}
	}

	return count;
}
int main(void) {

	setvbuf(stdout, NULL, _IONBF, 0);

	srand(time(NULL));

	char map[WIDTH][HEIGHT];

	int i, j;


	for (i = 0; i < WIDTH; i++) {

		for (j = 0; j < HEIGHT; j++) {
			map[i][j] = '.';
		}
	}

	generateMines(map);

	int countMines = 0;

	/*
	map[0][0] = '*';
	map[WIDTH-1][0] = '*';
	map[0][HEIGHT - 1] = '*';
	map[WIDTH-1][HEIGHT -1] = '*';

	map[6][0] = '*';
	map[0][6] = '*';
	map[11][5] = '*';
	map[6][29] = '*';
	map[6][15] = '*';
*/
	for (i = 0; i < WIDTH; i++) {
		countMines = 0;
		for (j = 0; j < HEIGHT; j++) {

			countMines = getCount(i, j, map);

			if (i == 0 && j == 0 && map[i][j] == '*') {
				countMines++;
				map[i+1][j] = countMines + '0';
				map[i+1][j+1] = countMines + '0';
				map[i][j+1] = countMines + '0';

				if (map[i+1][j] == '*' || map[i+1][j+1] || map[i][j+1] == '*')
					countMines++;
			}

			else if (i == WIDTH - 1 && j == 0 && map[i][j] == '*') {
				countMines++;
				map[i-1][j] = 1 + '0';
				map[i-1][j+1] = 1 + '0';
				map[i][j+1] = 1 + '0';

				if (map[i-1][j] == '*' || map[i-1][j+1] || map[i][j+1] == '*')
					countMines++;
			}

			else if (i == 0 && j == HEIGHT - 1 && map[i][j] == '*') {
				countMines++;
				map[i][j-1] = 1 + '0';
				map[i+1][j-1] = 1 + '0';
				map[i+1][j] = 1 + '0';

				if (map[i][j-1] == '*' || map[i+1][j-1] || map[i+1][j] == '*')
					countMines++;
			}

			else if (i == WIDTH - 1 && j == HEIGHT - 1 && map[i][j] == '*') {
				countMines++;
				map[i-1][j] = 1 + '0';
				map[i-1][j-1] = 1 + '0';
				map[i][j-1] = 1 + '0';

				if (map[i-1][j] == '*' || map[i-1][j-1] || map[i][j-1] == '*')
									countMines++;
			}

			else if (i > 0 && j == 0 && map[i][j] == '*') {

				countMines++;
				map[i-1][j] = countMines + '0';
				map[i-1][j+1] = countMines + '0';
				map[i][j+1] = countMines + '0';
				map[i+1][j] = countMines + '0';
				map[i+1][j+1] = countMines + '0';

				if (map[i-1][j] == '*' || map[i-1][j+1] || map[i][j+1] == '*' || map[i+1][j] || map[i+1][j+1])
									countMines++;
			}
			else if (i == 0 && j > 0 && map[i][j] == '*') {

				countMines++;
				map[i][j-1] = countMines + '0';
				map[i][j+1] = countMines + '0';
				map[i+1][j-1] = countMines + '0';
				map[i+1][j] = countMines + '0';
				map[i+1][j+1] = countMines + '0';

				if (map[i][j-1] == '*' || map[i][j+1] || map[i+1][j-1] == '*' || map[i+1][j] || map[i+1][j+1])
													countMines++;
			}

			else if (i == WIDTH - 1 && j > 0 && map[i][j] == '*') {

				countMines++;
				map[i][j-1] = countMines + '0';
				map[i][j+1] = countMines + '0';
				map[i-1][j-1] = countMines + '0';
				map[i-1][j] = countMines + '0';
				map[i-1][j+1] = countMines + '0';

				if (map[i-1][j] == '*' || map[i-1][j+1] || map[i][j+1] == '*' || map[i+1][j] || map[i+1][j+1])
													countMines++;
			}

			else if (i > 0 && j == HEIGHT - 1 && map[i][j] == '*') {

				countMines++;
				map[i-1][j] = countMines + '0';
				map[i+1][j] = countMines + '0';
				map[i-1][j-1] = countMines + '0';
				map[i][j-1] = countMines + '0';
				map[i+1][j-1] = countMines + '0';

				if (map[i-1][j] == '*' || map[i+1][j] || map[i-1][j-1] == '*' || map[i][j-1] || map[i+1][j-1])
													countMines++;
			}

			else if (i > 0 && j > 0 && i < WIDTH - 1 && j < HEIGHT - 1 && map[i][j] == '*'){

				countMines++;
				map[i-1][j-1] = countMines + '0';
				map[i-1][j] = countMines + '0';
				map[i-1][j+1] = countMines + '0';
				map[i][j-1] = countMines + '0';
				map[i][j+1] = countMines + '0';
				map[i+1][j-1] = countMines + '0';
				map[i+1][j] = countMines + '0';
				map[i+1][j+1] = countMines + '0';

				if (map[i-1][j-1] == '*' || map[i-1][j] || map[i-1][j+1] == '*' || map[i][j-1] || map[i][j+1] || map[i+1][j-1] == '*'
						|| map[i+1][j] == '*' || map[i=1][j+1] == '*')
													countMines++;


			}
		}
	}

	displayMap(map);
	return EXIT_SUCCESS;
}
