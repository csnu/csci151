/*
 ============================================================================
 Name        : MineSweeperGame.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define WIDTH 12
#define HEIGHT 30

void fillMap(char map[][HEIGHT]) {

	int i, j;

	for (i = 0; i < WIDTH; i++) {

		for (j = 0; j < HEIGHT; j++) {
			map[i][j] = '.';
		}
	}
}

void displayMap(char map[][HEIGHT]) {

	int i, j;

	for (i = 0; i < WIDTH; i++) {

		for (j = 0; j < HEIGHT; j++)
			printf("%c", map[i][j]);
		printf("\n");
	}
}

void generateMines(char map[][HEIGHT]) {

	int i;

	int xRandom, yRandom;

	for (i = 0; i < 10; i++) {

		 xRandom = rand() % WIDTH;
		 yRandom = rand() % HEIGHT;

		 if (map[xRandom][yRandom] != '.')
			 continue;
		 else
			 map[xRandom][yRandom] = '*';
	}
}

void addProximities(char map[][HEIGHT]) {
	int i, j;

	for (i = 0; i < WIDTH; i++) {

			for (j = 0; j < HEIGHT; j++) {



				if (i == 0 && j == 0 && map[i][j] == '*') {

					map[i+1][j] = '0';
					map[i+1][j+1] = '0';
					map[i][j+1] = '0';

				}

				else if (i == WIDTH - 1 && j == 0 && map[i][j] == '*') {

					map[i-1][j] = '0';
					map[i-1][j+1] = '0';
					map[i][j+1] = '0';


				}

				else if (i == 0 && j == HEIGHT - 1 && map[i][j] == '*') {

					map[i][j-1] = '0';
					map[i+1][j-1] = '0';
					map[i+1][j] = '0';


				}

				else if (i == WIDTH - 1 && j == HEIGHT - 1 && map[i][j] == '*') {

					map[i-1][j] = '0';
					map[i-1][j-1] = '0';
					map[i][j-1] = '0';


				}

				else if (i > 0 && j == 0 && map[i][j] == '*') {

					map[i-1][j] = '0';
					map[i-1][j+1] = '0';
					map[i][j+1] = '0';
					map[i+1][j] = '0';
					map[i+1][j+1] = '0';


				}
				else if (i == 0 && j > 0 && map[i][j] == '*') {


					map[i][j-1] = '0';
					map[i][j+1] = '0';
					map[i+1][j-1] = '0';
					map[i+1][j] = '0';
					map[i+1][j+1] = '0';

				}

				else if (i == WIDTH - 1 && j > 0 && map[i][j] == '*') {


					map[i][j-1] = '0';
					map[i][j+1] = '0';
					map[i-1][j-1] = '0';
					map[i-1][j] = '0';
					map[i-1][j+1] = '0';


				}

				else if (i > 0 && j == HEIGHT - 1 && map[i][j] == '*') {


					map[i-1][j] = '0';
					map[i+1][j] = '0';
					map[i-1][j-1] = '0';
					map[i][j-1] = '0';
					map[i+1][j-1] = '0';


				}

				else if (i > 0 && j > 0 && i < WIDTH - 1 && j < HEIGHT - 1 && map[i][j] == '*'){


					map[i-1][j-1] = '0';
					map[i-1][j] = '0';
					map[i-1][j+1] = '0';
					map[i][j-1] = '0';
					map[i][j+1] = '0';
					map[i+1][j-1] = '0';
					map[i+1][j] = '0';
					map[i+1][j+1] = '0';
				}
			}
		}


		for (i = 0; i < WIDTH; i++) {

				for (j = 0; j < HEIGHT; j++) {



					if (i == 0 && j == 0 && map[i][j] == '*') {

						map[i+1][j] = map[i+1][j] + 1;
						map[i+1][j+1] =  map[i+1][j+1] + 1;
						map[i][j+1] =  map[i][j+1] + 1;

					}

					else if (i == WIDTH - 1 && j == 0 && map[i][j] == '*') {

						map[i-1][j] =  map[i-1][j] + 1;
						map[i-1][j+1] =  map[i-1][j+1] + 1;
						map[i][j+1] =  map[i][j+1] + 1;


					}

					else if (i == 0 && j == HEIGHT - 1 && map[i][j] == '*') {

						map[i][j-1] = map[i][j-1] + 1;
						map[i+1][j-1] = map[i+1][j-1] + 1;
						map[i+1][j] = map[i+1][j] + 1;


					}

					else if (i == WIDTH - 1 && j == HEIGHT - 1 && map[i][j] == '*') {

						map[i-1][j] = map[i-1][j] + 1;
						map[i-1][j-1] = map[i-1][j-1] + 1;
						map[i][j-1] = map[i][j-1] + 1;


					}

					else if (i > 0 && j == 0 && map[i][j] == '*') {

						map[i-1][j] =  map[i-1][j] + 1;
						map[i-1][j+1] =  map[i-1][j+1] + 1;
						map[i][j+1] =  map[i][j+1] + 1;
						map[i+1][j] =  map[i+1][j] + 1;
						map[i+1][j+1] =  map[i+1][j+1] + 1;


					}
					else if (i == 0 && j > 0 && map[i][j] == '*') {


						map[i][j-1] = map[i][j-1] + 1;
						map[i][j+1] = map[i][j+1] + 1;
						map[i+1][j-1] = map[i+1][j-1] + 1;
						map[i+1][j] = map[i+1][j] + 1;
						map[i+1][j+1] = map[i+1][j+1] + 1;

					}

					else if (i == WIDTH - 1 && j > 0 && map[i][j] == '*') {


						map[i][j-1] = map[i][j-1] + 1;
						map[i][j+1] = map[i][j+1] + 1;
						map[i-1][j-1] = map[i-1][j-1] + 1;
						map[i-1][j] = map[i-1][j] + 1;
						map[i-1][j+1] = map[i-1][j+1] + 1;
					}

					else if (i > 0 && j == HEIGHT - 1 && map[i][j] == '*') {


						map[i-1][j] = map[i-1][j] + 1;
						map[i+1][j] = 	map[i+1][j] + 1;
						map[i-1][j-1] = map[i-1][j-1] + 1;
						map[i][j-1] = map[i][j-1] + 1;
						map[i+1][j-1] = map[i+1][j-1] + 1;


					}

					else if (i > 0 && j > 0 && i < WIDTH - 1 && j < HEIGHT - 1 && map[i][j] == '*'){


						map[i-1][j-1] = map[i-1][j-1] + 1;
						map[i-1][j] = map[i-1][j] + 1;
						map[i-1][j+1] = map[i-1][j+1] + 1;
						map[i][j-1] = map[i][j-1] + 1;
						map[i][j+1] = map[i][j+1] +1;
						map[i+1][j-1] = map[i+1][j-1] + 1;
						map[i+1][j] = map[i+1][j] + 1;
						map[i+1][j+1] = map[i+1][j+1] + 1;
					}
				}
			}
}


int main(void) {

	setvbuf(stdout, NULL, _IONBF, 0);

	srand(time(NULL));

	char map[WIDTH][HEIGHT];

	//int i, j;


	fillMap(map);

	generateMines(map);

	//int countMines = 0;

/*
	map[0][0] = '*';
	map[WIDTH-1][0] = '*';
	map[0][HEIGHT - 1] = '*';
	map[WIDTH-1][HEIGHT -1] = '*';

	map[0][16] = '*';
	map[2][10] = '*';
	map[5][11] = '*';
	map[6][11] = '*';
	map[5][25] = '*';
*/
	addProximities(map);

	displayMap(map);

	return EXIT_SUCCESS;
}
