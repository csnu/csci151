/*
 ============================================================================
 Name        : StringOutputFunctions.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void writeFile(char fileName[], char ssData[], char oStringData[][50], int lines) {

	FILE *file = fopen(fileName, "a");

	if (file == NULL)
		printf("NULL\n");

	fprintf(file, "%s\n", ssData);

	int i;
	for (i = 0; i < lines; i++) {
		fprintf(file, "%s\n", oStringData[i]);
	}

	fprintf(file, "\n");
	fclose(file);
}


int isPalindrome(char string[]) {

	int isPalindrome = 1;

	int low = 0;
	int high = strlen(string) - 1;

	while (low <= high) {

		if (string[low] != string[high]) {
			isPalindrome = 0;
			break;
		}
		low++;
		high--;
	}

	return isPalindrome;
}

char *getPalindrome(char string[]) {

	return isPalindrome(string) > 0 ? "Is_Palindrome" : "Not_Palindrome";
}

char *substring(char string[], int from, int to) {

	static char str[50];

	int i, j;

	for (i = from, j = 0; i < to; i++, j++) {
		str[j] = string[i];
	}

	return str;
}

_Bool isEqual(char string1[], char string2[]) {

	int i;

	if (strlen(string1) != strlen(string2))
		return 0;

	for (i = 0; i < strlen(string1); i++) {

		if (string1[i] != string2[i])
			return 0;
	}

	return 1;
}

int howManySubstrings(char ss[], char string[]) {

	int count = 0;

	int ssLength = strlen(ss);

	int i;

	for (i = 0; i < strlen(string) - ssLength + 1; i++) {

		//printf("%i\n", i);

		//printf("%i\n", i+ssLength);

		//printf("%s\n", substring(string, i, i+ssLength));

		if (isEqual(ss, substring(string, i, i + ssLength))) {
			count++;
			i = i + ssLength - 1;
			//printf("%i\n", i);
		}
	}

	return count;
}

int countFileLines(char fileName[]) {

	int numberOfLines = 0;

	FILE *file = fopen(fileName, "r");

	char ch;
	while ((ch = getc(file)) != EOF) {
		if (ch == '\n')
			numberOfLines++;
	}

	return numberOfLines;
}

void readFile(char fileName[], char matrix[][50]) {

	FILE *file = fopen(fileName, "r");

	int lines = 0;//countFileLines(fileName);

	int j = 0;
	char ch;
	while ((ch = getc(file)) != EOF) {

		//printf("%c", ch);
		if (ch == '\n') {
			matrix[lines][j] = '\0';
			lines++;
			j = 0;
			continue;
		}

		matrix[lines][j] = ch;
		//printf("%c", matrix[lines][j]);
		j++;
	}

	fclose(file);
}
/*
void checkSubstringPalindrome(char subStr[], char iStringData[][50], char oStringData[][50],
		int nrIOFileLines) {

	int i, j;

	int count;
	for (i = 0; i < nrIOFileLines; i++) {

		for (j = 0; j < 50; j++) {

			if (iStringData[i][j] != '\0')
				oStringData[i][j] = iStringData[i][j];
			else if (j > strlen(iStringData[i]))
				oStringData[i][j] = '\t';

		}
		count = howManySubstrings(subStr, iStringData[i]);


	}
}
*/

void copyElements(char iStringData[], char oStringData[], int beginIndex, int size) {

	int i, j;
	for (i = beginIndex, j = 0; i < size; i++, j++) {
		oStringData[i] = iStringData[j];
	}

}

void empty(char matrix[][50], int size) {

	int i, j;

			for (i = 0; i < size; i++) {

				for (j = 0; j < 50; j++)

					matrix[i][j] = '\0';
			}
}
void checkSubstringPalindrome(char subStr[], char iStringData[][50], char oStringData[][50],
		int nrIOFileLines) {

	empty(oStringData, nrIOFileLines);
	int i;//, j;

	int size;

	for (i = 0; i < nrIOFileLines; i++) {
		size = strlen(iStringData[i]);
		//printf("%i\n", size);
		copyElements(iStringData[i], oStringData[i], 0,  size);
		oStringData[i][size] = '\t';
		oStringData[i][size+1] = howManySubstrings(subStr, iStringData[i]) + '0';
		oStringData[i][size+2] = '\t';
		copyElements(getPalindrome(iStringData[i]), oStringData[i], size+3,
							size +3 + strlen(getPalindrome(iStringData[i])));

	}
//int j;
/*
	for (i = 0; i < nrIOFileLines; i++) {

		j = strlen(oStringData[i]);
		printf("%i\n", j);
		oStringData[i][j] = '\t';
	}

	for (i = 0; i < nrIOFileLines; i++) {

			j = strlen(oStringData[i]);

			oStringData[i][j] = howManySubstrings(subStr, iStringData[i]) + '0';
	}

	for (i = 0; i < nrIOFileLines; i++) {

			j = strlen(oStringData[i]);

			oStringData[i][j] = '\t';
	}

	for (i = 0; i < nrIOFileLines; i++) {

			j = strlen(oStringData[i]);


			copyElements(getPalindrome(iStringData[i]), oStringData[i], j,
					j + strlen(getPalindrome(iStringData[i])));
	}
*/
}

void display(char matrix[][50], int size) {
	int i, j;

		for (i = 0; i < size; i++) {

			for (j = 0; j < 50 ; j++) {

				printf("%c", matrix[i][j]);
			}
			printf("\n");
		}
}
int main(void) {

	setvbuf(stdout, NULL, _IONBF, 0);

	int lines1 = countFileLines("iStrings.txt");
	//printf("%i lines here\n", lines1);

	int lines2 = countFileLines("subStrings.txt");


	char iStringData[lines1][50];

	char subStringData[lines2][50];

	readFile("iStrings.txt", iStringData);

	readFile("subStrings.txt", subStringData);

	//display(iStringData, lines1);

	//display(subStringData, lines2);

	char oStringData[lines1][50];
	checkSubstringPalindrome(subStringData[0], iStringData, oStringData, lines1);

	//display(oStringData, lines1);

	writeFile("oStrings.txt", subStringData[0], oStringData, lines1);

	checkSubstringPalindrome(subStringData[1], iStringData, oStringData, lines1);

	writeFile("oStrings.txt", subStringData[1], oStringData, lines1);
	return EXIT_SUCCESS;
}
